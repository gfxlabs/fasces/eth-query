local param_cmd(image) = {
        cmd: |||
            /kaniko/executor
            --context ${CI_PROJECT_DIR}
            --cache=true
            --cache-repo="${CI_REGISTRY_IMAGE}/kaniko/cache"
            --dockerfile "${CI_PROJECT_DIR}/%(img)s.Dockerfile"
            --destination "${CI_REGISTRY_IMAGE}/%(img)s:${CI_COMMIT_SHORT_SHA}"
            --destination "${CI_REGISTRY_IMAGE}/%(img)s:latest"
        ||| % {img: image},
};
local param_job(image, depends=[]) = {
    stage: 'build',
    image: {
      name: 'gcr.io/kaniko-project/executor:debug',
      entrypoint: [''],
    },
    [if std.length(depends) > 0 then 'needs' else null]: depends,
    script: [
        'mkdir -p /kaniko/.docker',
        @'echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64)\"}}}" > /kaniko/.docker/config.json',
        std.strReplace(param_cmd(image).cmd,"\n"," ")
    ]
  };
{
  "query": param_job("query",[]),
}
