# template-golang

template for golang projects 

## things you will need to edit:

1. change the example folder to something else so its a different binary
2. edit the gitlab-ci to build the images you want to build
3. edit the go.mod to match your repo/package name


## example structure:

```
app/             ---  things to be compiled into binary go here
    example/     ---  files that should compile when you run go build ./app/example/*
        main.go  ---  just a go file with package main
    NAME/*       ---  files that should compile when you run go build ./app/NAME/*
common/          ---  things shared across apps
    package1/*   ---  some code
    package2/*
    package2/subpackage1/*  --- have fun with it
```
## Makefile

the makefile is setup to automatically compile things in the bin directory

you will have to make your own dockerfiles (we can def codegen these, but we might want to modify them, so no point generating them at compile time)

## gitlab-ci.yml

the gitlab config has two jobs

1. compiles all the apps using make apps
2. compiles the docker image example.Dockerfile and uploads it to gfxlabs/example:latest