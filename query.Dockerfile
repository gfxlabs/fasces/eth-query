# syntax=docker/dockerfile:1
FROM golang:1.17.8-alpine
RUN apk add make curl g++ libc-dev
WORKDIR /src
COPY common common
COPY go.mod go.mod
COPY go.sum go.sum
COPY Makefile Makefile

COPY app/example app/example

RUN go mod tidy
RUN make apps

FROM alpine:latest
WORKDIR /root/
COPY --from=0 /src/bin/example.exe ./main.exe
CMD ["./main.exe"]
