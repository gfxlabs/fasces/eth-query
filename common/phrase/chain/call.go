package chain

import (
	"context"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/gfxlabs/fasces/ethquery/common/abi"
	"gitlab.com/gfxlabs/fasces/ethquery/common/phrase"
	"golang.org/x/crypto/sha3"
)

var Call = &callBuilder{}

type callBuilder struct{}

// subject call is context for an eth call
type call struct {
	Target      common.Address
	BlockNumber *big.Int
	Name        string

	Args []abi.SolidityType

	builder *abi.Builder
	client  *ethclient.Client
}

func (b *callBuilder) Phrase(cl *ethclient.Client, adjectives ...phrase.Adjective[*call]) *phrase.Phrase[*call, abi.BuiltBytes] {
	return &phrase.Phrase[*call, abi.BuiltBytes]{
		Subject:    Call.Subject(cl),
		Adjectives: adjectives,
		Verb:       Call.Verb(),
	}
}

// subject creator for Call
func (b *callBuilder) Subject(cl *ethclient.Client) *call {
	return &call{
		builder: abi.NewBuilder(0),
		client:  cl,
	}
}

// verb creatie

func (b *callBuilder) Verb() phrase.Verb[*call, abi.BuiltBytes] {
	return func(c *call) (abi.BuiltBytes, error) {
		// construct the query itself
		names := make([]string, len(c.Args))
		for i, v := range c.Args {
			names[i] = strings.ToLower(v.Name())
		}
		b := sha3.NewLegacyKeccak256()
		b.Write([]byte(c.Name))
		b.Write([]byte("("))
		b.Write([]byte(strings.Join(names, ",")))
		b.Write([]byte(")"))
		sig := b.Sum(nil)[:4]
		c.builder.SetSignature(sig)
		for _, arg := range c.Args {
			c.builder.AddType(arg)
		}
		bb := c.builder.Build()
		msg := ethereum.CallMsg{
			To:   &c.Target,
			Data: bb.Bytes(),
		}
		res, err := c.client.CallContract(context.Background(), msg, c.BlockNumber)
		if err != nil {
			return nil, err
		}
		return abi.BuiltBytes(res), nil
	}
}

// adjectives for Call
func (b *callBuilder) SetAddress(addr string) phrase.Adjective[*call] {
	return func(c *call) (*call, error) {
		if !strings.HasPrefix(addr, "0x") {
			return c, fmt.Errorf("NewCall: %s", "expect input start with 0x")
		}
		if len(addr) < 42 {
			return c, fmt.Errorf("NewCall: %s", "input too short")
		}
		c.Target = common.HexToAddress(addr)
		return c, nil
	}
}

func (b *callBuilder) SetMethod(method string) phrase.Adjective[*call] {
	return func(c *call) (*call, error) {
		c.Name = method
		return c, nil
	}
}

func (b *callBuilder) AddInput(t, val string) phrase.Adjective[*call] {
	return func(c *call) (*call, error) {
		arg := abi.ReflectTypeValue(t, val)
		c.Args = append(c.Args, arg)
		return c, nil
	}
}

// helper to add in put
func (b *callBuilder) ReflectInput(val string) phrase.Adjective[*call] {
	return Call.AddInput("", val)
}
