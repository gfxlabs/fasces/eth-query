package chain

import (
	"context"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/gfxlabs/fasces/ethquery/common/abi"
)

var s = &ethclient.Client{}

func init() {
	s, _ = ethclient.Dial("https://mainnet.rpc.gfx.xyz")
}

func TestSubject(t *testing.T) {
	bn, err := s.BlockNumber(context.Background())
	if err != nil {
		t.Fatalf("failed fetching block number")
	}
	t.Logf("block number: %v", bn)
}

var addr_WETH = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2"

func TestCall(t *testing.T) {
	phrase := Call.Phrase(s,
		Call.SetAddress(addr_WETH),
		Call.SetMethod("decimals"),
	)
	bb, err := phrase.Say()
	if err != nil {
		t.Fatalf("error decimals: %s", err)
	}
	t.Logf("decimals: %s", bb.String())
}

func TestCallArgs(t *testing.T) {
	phrase := Call.Phrase(s,
		Call.SetAddress(addr_WETH),
		Call.SetMethod("balanceOf"),
		Call.ReflectInput(addr_WETH),
	)
	bb, err := phrase.Say()
	if err != nil {
		t.Fatalf("error callargs: %s", err)
	}
	t.Logf("balanceOf: %s", bb.String())

	rv := abi.NewIntNFromInt(256, 0)
	pars := AbiDecode.Phrase(bb,
		AbiDecode.AddArg(rv),
	)
	args, err := pars.Say()
	if err != nil {
		t.Fatalf("err parseargs: %s", err)
	}
	rbo, err := abi.MightSolidityType[*big.Int](args[0])
	if err != nil {
		t.Fatalf("err reflect: %s", err)
	}
	t.Logf("parsed value: %v, reflected value %v", rv.Val.Val.Uint64(), rbo.Uint64())
}
