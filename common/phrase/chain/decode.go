package chain

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/gfxlabs/fasces/ethquery/common/abi"
	"gitlab.com/gfxlabs/fasces/ethquery/common/phrase"
)

var AbiDecode = &abiDecodeParser[[]abi.SolidityType]{}

type abiDecodeParser[T []abi.SolidityType] struct{}

// subject abiDecode is context for an eth abiDecode
type abiDecode struct {
	Target      common.Address
	BlockNumber *big.Int
	Name        string

	Args   []abi.SolidityType
	parser *abi.Parser
	bts    abi.BuiltBytes
}

func (b *abiDecodeParser[T]) Phrase(bb abi.BuiltBytes, adjectives ...phrase.Adjective[*abiDecode]) *phrase.Phrase[*abiDecode, T] {
	return &phrase.Phrase[*abiDecode, T]{
		Subject:    AbiDecode.Subject(bb),
		Adjectives: adjectives,
		Verb:       b.Verb(),
	}
}

// subject creator for AbiDecode
func (b *abiDecodeParser[T]) Subject(bb abi.BuiltBytes) *abiDecode {
	return &abiDecode{
		parser: abi.NewParser(),
		bts:    bb,
	}
}

// verb create
func (b *abiDecodeParser[T]) Verb() phrase.Verb[*abiDecode, T] {
	return func(c *abiDecode) (T, error) {
		err := c.parser.Parse(c.bts)
		if err != nil {
			return *new(T), err
		}
		return c.parser.Values(), nil
	}
}

//adjectives
func (b *abiDecodeParser[T]) AddArg(arg abi.SolidityType) phrase.Adjective[*abiDecode] {
	return func(c *abiDecode) (*abiDecode, error) {
		c.parser.AddType(arg)
		return c, nil
	}
}

//adjectives
func (b *abiDecodeParser[T]) AddResult(t, val string) phrase.Adjective[*abiDecode] {
	return func(c *abiDecode) (*abiDecode, error) {
		arg := abi.ReflectTypeValue(t, val)
		c.parser.AddType(arg)
		return c, nil
	}
}

// helper to add in put
func (b *abiDecodeParser[T]) ReflectResult(val string) phrase.Adjective[*abiDecode] {
	return AbiDecode.AddResult("", val)
}
