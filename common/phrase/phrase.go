package phrase

type Verb[T, O any] func(T) (O, error)
type Adjective[T any] func(T) (T, error)

type Phrase[T any, O any] struct {
	Subject    T
	Adjectives []Adjective[T]
	Verb       Verb[T, O]
}

func (p *Phrase[T, O]) Say() (out O, err error) {
	for _, v := range p.Adjectives {
		p.Subject, err = v(p.Subject)
		if err != nil {
			return *new(O), err
		}
	}
	return p.Verb(p.Subject)
}
