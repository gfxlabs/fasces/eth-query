package sentence

type Result struct {
	String string
	Error  error
}
