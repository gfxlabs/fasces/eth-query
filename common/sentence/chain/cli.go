package chain

import (
	"fmt"
	"math/big"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/gfxlabs/fasces/ethquery/common/abi"
	"gitlab.com/gfxlabs/fasces/ethquery/common/phrase/chain"
	"gitlab.com/gfxlabs/fasces/ethquery/common/sentence"
	"gitlab.com/gfxlabs/fasces/ethquery/common/util/lexer"
)

var s = &ethclient.Client{}

func init() {
	s, _ = ethclient.Dial("https://mainnet.rpc.gfx.xyz")
}

type chainCommands struct {
	Query Query `cmd:"query" help:"query chain"`
}

type Query struct {
	Target  string `help:"target address e.g. 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2" optional:"" arg:""`
	Method  string `help:"method name e.g. balanceOf " optional:"" arg:""`
	Args    string `help:"call arguments, e.g: [0n123, uint256 400], default: []" default:"" optional:"" arg:""`
	Returns string `help:"return arguments, e.g: [uint256, uint256], default: uint256" optional:"" default:"uint256" arg:""`

	Scale            string   `help:"div numeric results by this number" optional:"" default:"1"`
	ScaleParse       *big.Int `kong:"-"`
	BlockNumber      string   `name:"bn" optional:"" help:"block number (default latest)" default:"latest"`
	BlockNumberParse *big.Int `kong:"-"`

	ReturnIndex int `name:"ri" help:"index of return argument" optional:"" default:"0"`
	ArrayIndex  int `name:"ai" help:"index of array if is array" optional:"" default:"0"`

	Result any `kong:"-"`
}

func (Q *Query) Run(r *sentence.Result) error {
	var err error
	defer func() {
		if err != nil {
			r.Error = err
		}
	}()
	// parse all arguments
	Q.BlockNumberParse = parseNumber(Q.BlockNumber)
	Q.ScaleParse = parseNumber(Q.Scale)

	if Q.ScaleParse == nil {
		return fmt.Errorf("invalid scale: %s", Q.Scale)
	}

	phrase := chain.Call.Phrase(s,
		chain.Call.SetAddress(Q.Target),
		chain.Call.SetMethod(Q.Method),
	)
	if Q.Args != "" {
		argArray := lexer.SplitArrayAtCommas(Q.Args)
		for _, v := range argArray {
			sv := strings.Split(strings.TrimSpace(v), " ")
			if len(sv) > 1 {
				phrase.Adjectives = append(phrase.Adjectives,
					chain.Call.AddInput(sv[0], sv[1]),
				)
			} else {
				phrase.Adjectives = append(phrase.Adjectives,
					chain.Call.ReflectInput(v),
				)
			}
		}
	}
	bb, err := phrase.Say()
	if err != nil {
		return err
	}
	parse := chain.AbiDecode.Phrase(bb)
	retArray := lexer.SplitArrayAtCommas(Q.Returns)
	for _, v := range retArray {
		dec := chain.AbiDecode.AddResult(v, "")
		if dec != nil {
			parse.Adjectives = append(parse.Adjectives,
				dec,
			)
		}
	}
	args, err := parse.Say()
	if err != nil {
		return err
	}
	results := []any{}
	for _, v := range args {
		res, err := abi.FromSolidityType(v)
		if err != nil {
			return err
		}
		switch cast := res.(type) {
		case *big.Int:
			fd := new(big.Float).Quo(new(big.Float).SetInt(cast), new(big.Float).SetInt(Q.ScaleParse))
			res, _ = fd.Float64()
		}
		results = append(results, res)
	}
	if len(results) <= Q.ReturnIndex {
		return fmt.Errorf("return arg out of range: %d", Q.ReturnIndex)
	}
	can := results[Q.ReturnIndex]
	switch cast := can.(type) {
	case []any:
		if len(cast) <= Q.ArrayIndex {
			return fmt.Errorf("idx out of range: %d", Q.ArrayIndex)
		}
		can = cast[Q.ArrayIndex]
	}
	r.String = fmt.Sprint(can)
	return nil
}

func parseNumber(s string) *big.Int {
	b := new(big.Int)
	if strings.Contains(s, "e") {
		sp := strings.Split(s, "e")
		if len(sp) == 2 {
			if p1, err := strconv.Atoi(sp[0]); err == nil {
				if p2, err := strconv.Atoi(sp[1]); err == nil {
					return new(big.Int).Mul(b.SetInt64(int64(p1)), new(big.Int).Exp(big.NewInt(10), big.NewInt(int64(p2)), nil))
				}
			}
		}
	}
	ans, ok := new(big.Int).SetString(s, 0)
	if !ok {
		return nil
	}
	return ans
}
