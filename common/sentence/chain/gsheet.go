package chain

import (
	"fmt"
	"strings"
)

type ChainQuery struct {
	c string

	Address     string
	MethodName  string
	BlockNumber string
}

func NewChainQuery(c string) *ChainQuery {
	return &ChainQuery{c: c}
}

func (q *ChainQuery) Content() string {
	return q.c
}
func (q *ChainQuery) Parse() error {
	// chain call format:
	// 0x046728da7cb8272284238bd3e47909823d63a58d@getUnderlyingPrice(address %a)(uint256)
	splt := strings.Split(q.c, ".")
	if len(splt) < 3 {
		return fmt.Errorf("not enough args")
	}
	q.Address = splt[0]

	return nil
}
