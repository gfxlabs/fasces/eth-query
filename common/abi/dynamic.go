package abi

import (
	"bytes"
	"fmt"
	"io"
	"strings"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"gitlab.com/gfxlabs/fasces/ethquery/common/util"
)

var _ SolidityType = (*ArrayType[SolidityType])(nil)

type ArrayType[T SolidityType] struct {
	Vals []T
}

func (t *ArrayType[T]) Value() []T {
	return t.Vals
}
func (t *ArrayType[T]) Encode(from int, w io.Writer) (int, error) {
	bd := NewBuilder(from)
	//write length
	n1, err := NewIntNFromInt(256, len(t.Vals)).Encode(0, w)
	if err != nil {
		return from, err
	}
	// add Values
	for _, v := range t.Vals {
		bd.AddType(v)
	}
	// now build
	res := bd.Build()
	n2, err := w.Write(res.Bytes())
	if err != nil {
		return from, err
	}
	_, _ = n1, n2
	return from + n1 + n2, nil
}

func (t *ArrayType[T]) Decode(r io.Reader, full []byte) error {
	// grab the offset
	offset := (&IntegerNType{Size: 256})
	err := offset.Decode(r, nil)
	if err != nil {
		return err
	}
	o := int(offset.Val.Uint64())
	if (o + 32) > len(full) {
		return fmt.Errorf("offset %d out of range (%d)", o, len(full))
	}
	// create a buffer beginning at the offset
	// and read the length
	obuf := bytes.NewBuffer(full[o:])
	bn, err := util.ReadInt32(obuf)
	if err != nil {
		return err
	}
	n := int(bn.Uint64())
	for i := 0; i < n; i++ {
		element := *new(T)
		err := element.Decode(r, full)
		if err != nil {
			return err
		}
	}
	return nil
}

func (t *ArrayType[T]) Name() string {
	if len(t.Vals) > 0 {
		return t.Vals[0].Name() + "[]"
	}
	return "uint256[]"
}

func (t *ArrayType[T]) Dynamic() bool { return true }

// string type
var _ SolidityType = (*StringType)(nil)

type StringType struct {
	Val string
}

func (t *StringType) Value() string { return t.Val }
func (t *StringType) Name() string  { return "string" }
func (t *StringType) Dynamic() bool { return true }
func (t *StringType) Encode(offset int, r io.Writer) (int, error) {
	bts := util.PackBytes([]byte(t.Val), len([]byte(t.Val)))
	return r.Write(bts)
}

func (t *StringType) Decode(r io.Reader, full []byte) error {
	// grab the offset
	offset := (&IntegerNType{Size: 256})
	err := offset.Decode(r, nil)
	if err != nil {
		return err
	}
	o := int(offset.Val.Uint64())
	if (o + 32) > len(full) {
		return fmt.Errorf("offset %d out of range (%d)", o, len(full))
	}
	// create a buffer beginning at the offset
	// and read the length
	obuf := bytes.NewBuffer(full[o:])
	bn, err := util.ReadInt32(obuf)
	if err != nil {
		return err
	}
	n := bn.Int64()
	// read N bytes
	exp := obuf.Bytes()
	if int(n) > len(exp) {
		return fmt.Errorf("offset %d out of range (%d)", o, len(full))
	}
	t.Val = string(exp[:n])
	return nil
}

// string type
var _ SolidityType = (*BytesType)(nil)

type BytesType struct {
	Val []byte
}

func (t *BytesType) Name() string  { return "bytes" }
func (t *BytesType) Value() []byte { return t.Val }
func (t *BytesType) Dynamic() bool { return true }
func (t *BytesType) Encode(offset int, r io.Writer) (int, error) {
	bts := util.PackBytes(t.Val, len(t.Val))
	return r.Write(bts)
}

func (t *BytesType) Decode(r io.Reader, full []byte) error {
	// grab the offset
	offset := (&IntegerNType{Size: 256})
	err := offset.Decode(r, nil)
	if err != nil {
		return err
	}
	o := int(offset.Val.Uint64())
	if (o + 32) > len(full) {
		return fmt.Errorf("offset %d out of range (%d)", o, len(full))
	}
	// create a buffer beginning at the offset
	// and read the length
	obuf := bytes.NewBuffer(full[o:])
	bn, err := util.ReadInt32(obuf)
	if err != nil {
		return err
	}
	n := bn.Int64()
	// read N bytes
	exp := obuf.Bytes()
	if int(n) > len(exp) {
		return fmt.Errorf("offset %d out of range (%d)", o, len(full))
	}
	t.Val = exp[:n]
	return nil
}

func NewBytesFromHex(s string) *BytesType {
	o := &BytesType{}
	if !strings.HasPrefix(s, "0x") {
		s = s + "0x"
	}
	o.Val, _ = hexutil.Decode(s)
	return o
}
