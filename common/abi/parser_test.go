package abi

import (
	"log"
	"testing"
)

func init() {

}

func TestParserPrimative1(t *testing.T) {
	b := NewBuilder(0)
	b.AddType(RTV("string", "testin"))
	b.AddType(RTV("string", "testing3"))

	built := b.Build()

	p := NewParser()
	p.AddType(RTV("string", "dog"))
	p.AddType(RTV("string", "dog"))
	err := p.Parse(built.Bytes())
	if err != nil {
		t.Fatalf("parser: %s", err)
	}
	if c, ok := p.args[0].(*StringType); ok {
		if c.Val != "testin" {
			log.Printf("expected %+v to be %s", p.args[0], "testin")
		}
	} else {
		log.Printf("expected %+v to be (*StringType)", p.args[0])
	}

	if c, ok := p.args[1].(*StringType); ok {
		if c.Val != "testing3" {
			log.Printf("expected %+v to be %s", p.args[1], "testing3")
		}
	} else {
		log.Printf("expected %+v to be (*StringType)", p.args[1])
	}
}

func TestParserLongString1(t *testing.T) {
	b := NewBuilder(0)
	longstring := "asyudg67iasdgastyud67tudtyuasdastyudtyuasfdyutasdyutasfdyutasdtyuasfdytuasgtyudasfyutdgastyudgasyutdgasyudguastydgtyuasgdtasyugdtyas"
	b.AddType(RTV("string", longstring))
	b.AddType(RTV("string", "testing3"))

	built := b.Build()

	p := NewParser()
	p.AddType(RTV("string", "dog"))
	p.AddType(RTV("string", "dog"))
	err := p.Parse(built.Bytes())
	if err != nil {
		t.Fatalf("parser: %s", err)
	}
	if c, ok := p.args[0].(*StringType); ok {
		if c.Val != longstring {
			log.Printf("expected %+v to be %s", p.args[0], longstring)
		}
	} else {
		log.Printf("expected %+v to be (*StringType)", p.args[0])
	}

	if c, ok := p.args[1].(*StringType); ok {
		if c.Val != "testing3" {
			log.Printf("expected %+v to be %s", p.args[1], "testing3")
		}
	} else {
		log.Printf("expected %+v to be (*StringType)", p.args[1])
	}

}
