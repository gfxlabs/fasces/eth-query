package abi

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/gfxlabs/fasces/ethquery/common/util/lexer"
)

func MustSolidityType[T any](t SolidityType) T {
	v, err := MightSolidityType[T](t)
	if err != nil {
		panic(err)
	}
	return v
}

func MightSolidityType[T any](t SolidityType) (T, error) {
	c1, err := FromSolidityType(t)
	if err != nil {
		return *new(T), nil
	}
	c2, ok := c1.(T)
	if !ok {
		return *new(T), fmt.Errorf("could not assert %s to (%v)", t.Name(), *new(T))
	}
	return c2, nil
}

func FromSolidityType(t SolidityType) (any, error) {
	switch cast := t.(type) {
	case *BytesType:
		return cast.Val, nil
	case *StringType:
		return cast.Val, nil
	case *ArrayType[SolidityType]:
		out := make([]any, 0, 1)
		for _, v := range cast.Vals {
			if a, err := FromSolidityType(v); err == nil {
				out = append(out, a)
			}
		}
		return out, nil
	case *ValueType[*IntegerNType]:
		return cast.Val.Val, nil
	case *ValueType[*BytesNType]:
		return cast.Val.Val, nil
	case *ValueType[*BytesLeftType]:
		return cast.Val.Val, nil
	case *ValueType[*AddressType]:
		return cast.Val.Val, nil
	case *ValueType[*NilType]:
		return nil, nil
	case *ValueType[PrimativeType]:
		return FromPrimativeType(cast.Val)
	case PrimativeType:
		return FromPrimativeType(cast)
	default:
		return nil, fmt.Errorf("unknown valtype %s: %+v, %v", t.Name(), t, reflect.TypeOf(cast))
	}
}

func FromPrimativeType(t PrimativeType) (any, error) {
	switch cast := t.(type) {
	case *IntegerNType:
		return cast.Val, nil
	case *BytesNType:
		return cast.Val, nil
	case *BytesLeftType:
		return cast.Val, nil
	case *AddressType:
		return cast.Val, nil
	case *NilType:
		return nil, nil
	default:
		return nil, fmt.Errorf("unknown primative %s: %+v, %v", cast.Name(), t, reflect.TypeOf(cast))
	}
}

func ReflectValue(s string) SolidityType {
	s = strings.TrimSpace(s)
	out := new(ValueType[PrimativeType])
	if strings.HasPrefix(s, "[") || strings.HasPrefix(s, "{") {
		// array, no nested
		if strings.HasSuffix(s, "]") {
			s = strings.TrimPrefix(s, "[")
			s = strings.TrimSuffix(s, "]")
			splt := lexer.SplitArrayAtCommas(s)
			return ReflectValueArray(splt...)
		}
		// struct, return nil
		out.Val = &NilType{}
		return out
	}
	// ok its not an array
	// does it start with 0x?
	if strings.HasPrefix(s, "0x") {
		// ok it's either a byte slice, fixed bytes, or an address
		if len(s) == 42 {
			// 42 characters, or 20 bytes, signifies an address
			out.Val = NewAddressFromString(s)
			return out
		}
		// ok well it's not an address. if the length is over 32, it's fixed bytes for sure
		if len(s) > (32*2 + 2) {
			return NewBytesFromHex(s)
		}
		// ok well it's under 32 bytes. assume its fixed bytes
		out.Val = NewBytesNFromHex(s)
		return out
	}

	// is it a 0n (number with hex prefix?)
	if len(s) <= 66 {
		if strings.HasPrefix(s, "0n") {
			s = strings.ReplaceAll(s, "0n", "0x")
			return NewIntNFromString(256, s)
		}
	}
	// try to turn it into a number
	num, err := strconv.Atoi(s)
	if err == nil {
		return NewIntNFromInt(256, num)
	}
	// ok its not a number, just encode it to a string
	tp := &StringType{Val: s}
	return tp
}

func RV(s string) SolidityType {
	return ReflectValue(s)
}

func ReflectValueArray(Vals ...string) *ArrayType[SolidityType] {
	out := *new(ArrayType[SolidityType])
	for _, Val := range Vals {
		vt := ReflectValue(Val)
		out.Vals = append(out.Vals, vt)
	}
	return &out
}

func RVA(Vals ...string) *ArrayType[SolidityType] {
	return ReflectValueArray(Vals...)
}

func breakType(s string) (string, int) {
	re := regexp.MustCompile(`[-]?\d[\d,]*[\.]?[\d{2}]*`)
	fas := re.FindAllString(s, -1)
	if len(fas) < 1 {
		return s, 0
	}
	total, _ := strconv.Atoi(fas[0])
	return strings.ReplaceAll(s, fas[0], ""), total
}

func ReflectTypeArray(t string, Vals ...string) *ArrayType[SolidityType] {
	out := *new(ArrayType[SolidityType])
	t = strings.ToLower(t)
	for _, Val := range Vals {
		vt := ReflectTypeValue(t, Val)
		out.Vals = append(out.Vals, vt)
	}
	return &out
}

func RTA(t string, Vals ...string) *ArrayType[SolidityType] {
	return ReflectTypeArray(t, Vals...)
}

func ReflectTypeValue(t, Val string) SolidityType {
	out := new(ValueType[PrimativeType])
	if t == "" {
		return ReflectValue(Val)
	}
	t = strings.TrimSpace(strings.ToLower(t))
	if strings.HasPrefix(t, "int") || strings.HasPrefix(t, "uint") {
		_, sz := breakType(t)
		return NewIntNFromString(sz, Val)
	} else if strings.HasPrefix(t, "bytes") {
		_, sz := breakType(t)
		tp := NewBytesNFromString(sz, Val)
		out.Val = tp
	} else {
		switch t {
		case "string":
			tp := &StringType{Val: Val}
			return tp
		default: // unknown, return empty
			out.Val = &NilType{}
			return out
		}
	}
	return out
}

// an alias
func RTV(t, Val string) SolidityType {
	return ReflectTypeValue(t, Val)
}
