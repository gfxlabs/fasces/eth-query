package abi

import (
	"bytes"
	"encoding/hex"
	"strings"
)

// builder
type Builder struct {
	free int
	sig  [4]byte
	args []SolidityType

	sigSet bool
}

func NewBuilder(free int) *Builder {
	return &Builder{
		free: free,
		args: make([]SolidityType, 0, 1),
	}
}
func (h *Builder) SetSignature(s []byte) {
	if len(s) < 4 {
		return
	}
	h.sig = [4]byte{s[0], s[1], s[2], s[3]}
	h.sigSet = true
}

func (h *Builder) AddValue(v ValueType[PrimativeType]) {
	h.args = append(h.args, &v)
}

func (h *Builder) AddType(t SolidityType) {
	h.args = append(h.args, t)
}

func (h *Builder) Build() *BuiltBytes {
	header := new(bytes.Buffer)
	body := new(bytes.Buffer)
	if h.sigSet {
		header.Write(h.sig[:])
	}
	// grab the current free memory points
	h.free = len(h.args) * 32
	// loop to fill the elements
	for _, v := range h.args {
		if v.Dynamic() {
			// mark value as current offset
			n1, err := NewIntNFromInt(256, h.free).Encode(0, header)
			if err != nil {
				continue
			}
			_ = n1
			// write body
			n2, err := v.Encode(0, body)
			if err != nil {
				continue
			}
			//advance free mem cursor
			h.free = h.free + n2
		} else {
			_, err := v.Encode(0, header) // write the value to header
			if err != nil {
				continue
			}
		}
	}
	body.WriteTo(header)
	final := BuiltBytes(header.Bytes())
	return &final
}

// helper output type
type BuiltBytes []byte

// return underlying slice
func (b *BuiltBytes) Bytes() []byte {
	return *b
}

func (b *BuiltBytes) String() string {
	bd := new(strings.Builder)
	bln := len(b.Bytes())
	start := 0
	if bln%32 != 0 {
		bd.WriteString(hex.EncodeToString(b.Bytes()[0:4]))
		start = 4
	}
	bd.WriteString("\n")
	for i := start; i < bln; i = i + 32 {
		target := i + 32
		if target > bln {
			target = bln
		}
		if i == target {
			break
		}
		bd.WriteString(hex.EncodeToString(b.Bytes()[i:target]) + "\n")
	}
	return bd.String()
}
