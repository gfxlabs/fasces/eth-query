package abi

import (
	"io"
	"math/big"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/common/math"
	"gitlab.com/gfxlabs/fasces/ethquery/common/util"
)

// uintN type
var _ PrimativeType = (*IntegerNType)(nil)

type IntegerNType struct {
	Size int
	Val  *big.Int
}

func (t *IntegerNType) Name() string {
	suffix := ""
	if t.Size != 8 {
		suffix = strconv.Itoa(t.Size)
	}
	return "uint" + suffix
}

func (t *IntegerNType) Encode(offset int, r io.Writer) (int, error) {
	return r.Write(math.PaddedBigBytes(math.U256(t.Val), (t.Size+7)/8))
}

func (t *IntegerNType) Decode(r io.Reader, full []byte) error {
	// read 32 bytes
	num := make([]byte, 32)
	_, err := io.ReadFull(r, num)
	if err != nil {
		return err
	}
	t.Val = util.ByteToInt(num)
	return nil
}
func (t *IntegerNType) DecodeRaw(bts []byte) error {
	util.ByteToInt(bts)
	return nil
}

func (t *IntegerNType) RawValue() any {
	return t.Val
}

func NewIntNFromString(Size int, Val string) (t *ValueType[*IntegerNType]) {
	base := 0
	Val = strings.ToLower(Val)
	if strings.HasPrefix(Val, "0x") {
		Val = strings.TrimPrefix(Val, "0x")
		base = 16
	}
	bi, ok := new(big.Int).SetString(Val, base)
	if !ok {
		bi = big.NewInt(0)
	}
	return &ValueType[*IntegerNType]{&IntegerNType{Size: Size, Val: bi}}
}

func NewIntNFromInt(Size int, Val int) (t *ValueType[*IntegerNType]) {
	if Size == 0 {
		Size = 256
	}
	if Size > 256 {
		Size = 256
	}
	return &ValueType[*IntegerNType]{&IntegerNType{Size: Size, Val: big.NewInt(int64(Val))}}
}

// fixed bytes type
type BytesNType struct {
	Size int
	Val  []byte
}

func (t *BytesNType) Name() string {
	suffix := ""
	if t.Size != 8 {
		suffix = strconv.Itoa(t.Size)
	}
	return "bytes" + suffix
}

func NewBytesNFromString(Size int, Val string) (t *BytesNType) {
	if Size > 32 {
		Size = 32
	}
	return &BytesNType{Size: Size, Val: []byte(Val)}
}

func NewBytesNFromArray(Size int, Val []byte) (t *BytesNType) {
	if Size > 32 {
		Size = 32
	}
	return &BytesNType{Size: Size, Val: Val}
}
func NewBytesNFromHex(s string) *BytesNType {
	if !strings.HasPrefix(s, "0x") {
		s = s + "0x"
	}
	Val, _ := hexutil.Decode(s)
	return NewBytesNFromArray(len(Val), Val)
}

func (t *BytesNType) RawValue() any {
	return t.Val
}

func (t *BytesNType) Encode(offset int, r io.Writer) (int, error) {
	// truncate left
	if t.Size > len(t.Val) {
		t.Val = t.Val[:t.Size]
	}
	return r.Write(common.RightPadBytes(t.Val, 32))
}

func (t *BytesNType) Decode(r io.Reader, full []byte) error {
	// truncate left
	t.Val = make([]byte, t.Size)
	// read n bytes
	_, err := io.ReadFull(r, t.Val)
	if err != nil {
		return err
	}

	return nil
}

// fixed bytes type
type BytesLeftType struct {
	Size int
	Val  []byte
}

func (t *BytesLeftType) Name() string {
	suffix := ""
	if t.Size != 8 {
		suffix = strconv.Itoa(t.Size)
	}
	return "bytes" + suffix
}

func (t *BytesLeftType) Encode(offset int, r io.Writer) (int, error) {
	// truncate left
	if t.Size > len(t.Val) {
		t.Val = t.Val[:t.Size]
	}
	return r.Write(common.LeftPadBytes(t.Val, 32))
}

func (t *BytesLeftType) Decode(r io.Reader, full []byte) error {
	// truncate left
	t.Val = make([]byte, t.Size)
	// read n bytes
	_, err := io.ReadFull(r, t.Val)
	if err != nil {
		return err
	}

	return nil
}

func (t *BytesLeftType) RawValue() any {
	return t.Val
}

// address type
var _ PrimativeType = (*AddressType)(nil)

// address type
type AddressType ValueType[*BytesLeftType]

func NewAddressFromString(Val string) (t *AddressType) {
	if !strings.HasPrefix(Val, "0x") {
		Val = "0x" + Val
	}
	bt, err := hexutil.Decode(Val)
	if err != nil {
		bt = []byte{}
	}
	out := &AddressType{}
	out.Val = &BytesLeftType{}
	out.Val.Val = bt
	return out
}

func (t *AddressType) RawValue() any {
	return t.Val
}

func (t *AddressType) Encode(offset int, r io.Writer) (int, error) {
	t.Val.Size = 20
	return t.Val.Encode(offset, r)
}

func (t *AddressType) Decode(r io.Reader, full []byte) error {
	t.Val.Size = 20
	return t.Val.Decode(r, full)
}

func (t *AddressType) Name() string { return "address" }

var _ PrimativeType = (*NilType)(nil)

type NilType struct{}

func (t *NilType) Encode(offset int, r io.Writer) (int, error) {
	return 0, nil
}
func (t *NilType) Decode(r io.Reader, full []byte) error {
	return nil
}
func (t *NilType) Name() string { return "nil" }

func (t *NilType) RawValue() any {
	return nil
}
