package abi

import (
	"io"
)

type PrimativeType interface {
	Name() string
	Encode(offset int, w io.Writer) (int, error)
	Decode(r io.Reader, f []byte) error

	RawValue() any
}

type SolidityType interface {
	Name() string
	Encode(offset int, w io.Writer) (int, error)
	Decode(r io.Reader, f []byte) error

	Dynamic() bool
}

type ValueType[T PrimativeType] struct {
	Val T
}

func (t *ValueType[T]) Name() string {
	return t.Val.Name()
}
func (t *ValueType[T]) Encode(offset int, r io.Writer) (int, error) {
	return t.Val.Encode(offset, r)
}
func (t *ValueType[T]) Decode(r io.Reader, full []byte) error {
	return t.Val.Decode(r, full)
}
func (t *ValueType[T]) Dynamic() bool {
	return false
}
func (t *ValueType[T]) Value() T {
	return t.Value()
}
