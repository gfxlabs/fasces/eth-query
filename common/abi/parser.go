package abi

import (
	"bytes"
)

// builder
type Parser struct {
	args []SolidityType
}

func NewParser() *Parser {
	return &Parser{
		args: make([]SolidityType, 0, 1),
	}
}

func (h *Parser) AddValue(v ValueType[PrimativeType]) {
	h.args = append(h.args, &v)
}

func (h *Parser) AddType(t SolidityType) {
	h.args = append(h.args, t)
}

func (h *Parser) Values() []SolidityType {
	return h.args
}

func (h *Parser) Parse(bs []byte) error {
	bsCopy := make([]byte, len(bs))
	copy(bsCopy, bs)
	buf := bytes.NewBuffer(bsCopy)
	for _, v := range h.args {
		err := v.Decode(buf, bsCopy)
		if err != nil {
			return err
		}
	}
	return nil
}
