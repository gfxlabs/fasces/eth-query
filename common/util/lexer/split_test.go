package lexer

import (
	"fmt"
	"testing"
)

var commaCases = [][2]string{
	{"[1,2],3", "[[1,2] 3]"},
	{"1,2,3", "[1 2 3]"},
	{"1,[2,3],4", "[1 [2,3] 4]"},
}

func TestSplitArrayAtCommas(t *testing.T) {
	for _, v := range commaCases {
		res := SplitArrayAtCommas(v[0])
		expect := fmt.Sprintf("%s", res)
		if expect != v[1] {
			t.Fatalf("expected %s to be %s", expect, v[1])
		}
	}
	return
}
