package lexer

import "strings"

// SplitAtCommas split s at commas, ignoring commas in strings.
func SplitArrayAtCommas(s string) []string {
	s = strings.Trim(s, "\"")
	res := []string{}
	var beg int
	var inString bool
	for i := 0; i < len(s); i++ {
		if s[i] == ',' && !inString {
			res = append(res, s[beg:i])
			beg = i + 1
		} else if s[i] == '[' {
			if i > 0 {
				if s[i-1] == '\\' {
					continue
				}
			}
			if !inString {
				inString = true
			}
		} else if s[i] == ']' {
			if i > 0 {
				if s[i-1] == '\\' {
					continue
				}
			}
			if inString {
				inString = false
			}
		}
	}
	res = append(res, s[beg:])
	for i, v := range res {
		res[i] = strings.TrimSpace(v)
	}
	return res
}
