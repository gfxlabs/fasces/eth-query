package util

import (
	"io"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
)

var (
	// MaxUint256 is the maximum value that can be represented by a uint256.
	MaxUint256 = new(big.Int).Sub(new(big.Int).Lsh(common.Big1, 256), common.Big1)
	// MaxInt256 is the maximum value that can be represented by a int256.
	MaxInt256 = new(big.Int).Sub(new(big.Int).Lsh(common.Big1, 255), common.Big1)
)

func PadNumber32(i int) int {
	if n := i % 32; n != 0 {
		i = i + (32 - n)
	}
	return i
}

func PackBytes(bytes []byte, l int) []byte {
	ln := math.U256Bytes(big.NewInt(int64(l)))
	return append(ln, common.RightPadBytes(bytes, (l+31)/32*32)...)
}

func ByteToInt(bs []byte) *big.Int {
	ret := new(big.Int).SetBytes(bs)
	if ret.Bit(255) == 1 {
		ret.Add(MaxUint256, new(big.Int).Neg(ret))
		ret.Add(ret, common.Big1)
		ret.Neg(ret)
	}
	return ret
}

func ReadFull32(r io.Reader) ([]byte, error) {
	num := make([]byte, 32)
	_, err := io.ReadFull(r, num)
	if err != nil {
		return nil, err
	}
	return num, nil
}

func ReadInt32(r io.Reader) (*big.Int, error) {
	num := make([]byte, 32)
	_, err := io.ReadFull(r, num)
	if err != nil {
		return nil, err
	}
	return ByteToInt(num), nil
}
