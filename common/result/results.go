package result

type Result[V any] struct {
	Value V
	Err   error
}

func New[V any](v V) *Result[V] {
	return &Result[V]{Value: v}
}
