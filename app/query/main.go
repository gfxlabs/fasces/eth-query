package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"git.tuxpa.in/a/zlog/log"
	"github.com/alecthomas/kong"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"gitlab.com/gfxlabs/fasces/ethquery/common/sentence"
	"gitlab.com/gfxlabs/fasces/ethquery/common/sentence/chain"
	"golang.org/x/oauth2/google"
	"gopkg.in/Iwark/spreadsheet.v2"
)

type Client struct {
	S             *spreadsheet.Service
	TrackedSheets map[string]struct{}

	sync.RWMutex
}

var defaultSheets = []string{
	"1Meyl5eyU-Td6NfyL_jWSADR6etDRQc5-lCTmbnVg6eo",
}

func NewClient(file string) (*Client, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	conf, err := google.JWTConfigFromJSON(data, spreadsheet.Scope)
	if err != nil {
		return nil, err
	}
	client := conf.Client(context.Background())

	srv := spreadsheet.NewServiceWithClient(client)
	out := &Client{
		TrackedSheets: map[string]struct{}{},
		S:             srv,
	}
	for _, v := range defaultSheets {
		out.TrackedSheets[v] = struct{}{}
	}
	go out.run()
	return out, nil
}

func (o *Client) run() {
	for k := range o.TrackedSheets {
		err := o.UpdateSheet(k)
		if err != nil {
			log.Println(err)
		}
	}
	tick := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-tick.C:
			for k := range o.TrackedSheets {
				err := o.UpdateSheet(k)
				if err != nil {
					log.Println(err)
				}
			}
		}
		tick.Reset(5 * time.Second)
	}
}

var sheetQueryIdentifier = "_query"
var sheetResultIdentifier = "_result"

func (o *Client) UpdateSheet(s string) error {
	sheet, err := o.S.FetchSpreadsheet(s)
	if err != nil {
		return err
	}
	for _, v := range sheet.Sheets {
		if strings.Contains(v.Properties.Title, sheetQueryIdentifier) {
			rst := strings.ReplaceAll(v.Properties.Title, sheetQueryIdentifier, sheetResultIdentifier)
			resultSheet, err := sheet.SheetByTitle(rst)
			if err != nil {
				err = o.S.AddSheet(&sheet, spreadsheet.SheetProperties{
					Title: rst,
				})
				if err != nil {
					return err
				}
				resultSheet, err = sheet.SheetByTitle(rst)
				if err != nil {
					return err
				}
			}
			for _, row := range v.Rows {
				for _, cell := range row {
					cp := &cell
					ok, err := o.CheckCell(s, cp)
					if err != nil {
						cell.Note = err.Error()
						cell.Value = cell.Note
					}
					if err == nil {
						cell.Note = ""
					}
					if ok {
						resultSheet.Update(int(cp.Row), int(cp.Column), cp.Value)
						v.UpdateNote(int(cp.Row), int(cp.Column), cp.Note)
					}
				}
			}
			resultSheet.Synchronize()
			v.Synchronize()
		}
	}
	return nil
}

var queryRunner, _ = kong.New(&QueryStruct,
	kong.Name("sheet query tool:\n"),
	kong.Exit(func(int) {}))

func (o *Client) CheckCell(id string, cell *spreadsheet.Cell) (bool, error) {
	csv := cell.Value
	if strings.HasPrefix(csv, "query") {
		result := new(sentence.Result)
		ctx, err := queryRunner.Parse(strings.Split(csv, " "))
		if err != nil {
			return true, err
		}
		err = ctx.Run(result)
		if err != nil {
			return true, err
		}
		cell.Note = result.String
		cell.Value = cell.Note
		return true, nil
	}
	return false, nil
}

var CliStruct struct {
	Query chain.Query `cmd:"query" help:"chain query commands"`

	Serve struct {
		Sheets Sheets `cmd:"sheets" help:"gsheet server"`
	} `cmd:"serve" help:"serve servers"`
}

var QueryStruct struct {
	Query chain.Query `cmd:"query" help:"--help for help"`
}

func main() {
	result := new(sentence.Result)
	ctx := kong.Parse(&CliStruct)
	err := ctx.Run(result)
	if err != nil {
		log.Println(err)
		return
	}
	if result.Error != nil {
		log.Println(err)
		return
	}
	fmt.Print(result.String)
}

type Sheets struct {
	CredentialFile string `cmd:"credentials" aliases:"creds" optional:""`
}

func (S *Sheets) Run() error {
	if S.CredentialFile == "" {
		S.CredentialFile = os.Getenv("GOOGLE_CREDENTIAL_FILE")
	}
	if S.CredentialFile == "" {
		S.CredentialFile = "./credentials.json"
	}
	sc, err := NewClient(S.CredentialFile)
	if err != nil {
		return err
	}
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	})
	r.Get("/addsheet", func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "sheet_id")
		if id == "" {
			w.Write([]byte("OK"))
			return
		}
		sc.Lock()
		sc.TrackedSheets[id] = struct{}{}
		sc.Unlock()
		w.Write([]byte(id))
	})
	log.Debug().Str("starting", "sheet_tool").Interface("port", ":3000").Msg("")
	err = http.ListenAndServe(":3000", r)
	if err != nil {
		log.Println(err)
	}
	return nil
}
